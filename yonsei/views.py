from django.shortcuts import render
from django.utils import timezone
from datetime import timedelta
from rest_framework.decorators import api_view, parser_classes
from rest_framework import generics, status
from rest_framework.response import Response

from meals.models import Menu, Restaurant
from meals.serializers import MenuSerializer

KUKJE_RESTAURANTS = ["Y-플라자", "송도1학사", "송도2학사"]
STUDENT_RESTAURANTS = ["카페테리아(맛나샘)", "푸드코트(부를샘)"]
HANKYUNG_RESTAURANTS = ["한경관(교직원식당)"]

def get_or_none(model, *args, **kwargs):
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None

@api_view(['GET'])
def menu(request):
    today = timezone.localtime(timezone.now()).date()
    tomorrow = (timezone.localtime(timezone.now()).today() + timedelta(days=1)).date()
    menu_data = { "today": { "date": today.__str__(), "menus": [] }, "tomorrow": { "date": tomorrow.__str__(), "menus": [] } }
    for restaurant_name in KUKJE_RESTAURANTS + STUDENT_RESTAURANTS + HANKYUNG_RESTAURANTS:
        restaurant = Restaurant.objects.get(name=restaurant_name)
        today_menu = get_or_none(Menu, restaurant=restaurant, date=today)
        if today_menu:
            menu_data["today"]["menus"].append(MenuSerializer(today_menu).data)
        else:
            menu_data["today"]["menus"].append({"restaurant": { "name": restaurant_name }, "meals": []})
        tomorrow_menu = get_or_none(Menu, restaurant=restaurant, date=tomorrow)
        if tomorrow_menu:
            menu_data["tomorrow"]["menus"].append(MenuSerializer(tomorrow_menu).data)
        else:
            menu_data["tomorrow"]["menus"].append({"restaurant": { "name": restaurant_name }, "meals": []})
    return Response(data=menu_data)


